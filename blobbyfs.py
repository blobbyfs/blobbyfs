""" A fuse filesystem which returns an empty file for any path which
ends with ".blob"

This is intended to help Plone developers who want to use a production
Data.fs without copying all the blob files. The blob storage directory
can be mounted as follows:
$ sudo modprobe fuse
$ python blobby.py var/blobstorage

For any path which ends in ".blob" an empty file will be returned:
$ file var/blobstorage/any/thing/at/all.blob
var/blobstorage/any/thing/at/all.blob: empty
"""

import fuse
import stat
import time

fuse.fuse_python_api = (0, 2)


class BlobbyFS(fuse.Fuse):

    def __init__(self, *args, **kw):
        fuse.Fuse.__init__(self, *args, **kw)
        print 'Init complete.'

    def read ( self, path, length, offset ):
        print '*** read', path, length, offset

    def getattr(self, path):
        print "path: %s" %path
        st = fuse.Stat()
        st.st_atime = int(time.time())
        st.st_mtime = st.st_atime
        st.st_ctime = st.st_atime

        if path.endswith(".blob"):
            st.st_mode = stat.S_IFREG | 0755
            st.st_nlink = 1
            st.st_size = 1000
        else:
            st.st_mode = stat.S_IFDIR | 0755
            st.st_nlink = 2
        return st


if __name__ == "__main__":
    fs = BlobbyFS()
    fs.parse(errex=1)
    fs.main()
